package muresan.emma.simina.lab2.ex3;
import java.util.Scanner;

public class primenb {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int A = scanner.nextInt();
        int B = scanner.nextInt();

        int count = 0;
        boolean ok;

        for( int i = A; i <= B ; i++ ){
            if( i < 2 ) continue;

            ok = true;
            for(int j = 2; j <= i/2; j++)
                if( i % j == 0) {
                    ok = false;
                    break;
                }
            if( ok == true ){
                count++;
                System.out.print(i + " ");
            }
        }
        System.out.println();
        System.out.println("Prime numbers: " + count);
    }
}
