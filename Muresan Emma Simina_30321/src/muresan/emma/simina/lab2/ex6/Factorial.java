package muresan.emma.simina.lab2.ex6;
import java.util.Scanner;

public class Factorial {

    public int recursive(int N){
        if(N > 1) return N*recursive(N-1);
        else return 1;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int f = 1;
        //1) non recursive

        for(int i = 2 ; i <= N ; i++ )
            f = f * i;
        System.out.println("Non-recursive: " + f);

        //2) recursive
        f = 1;
        Factorial ff = new Factorial();
        f = ff.recursive(N);
        System.out.println("Recursive: " + f);
    }
}
