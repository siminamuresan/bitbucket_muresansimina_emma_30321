package muresan.emma.simina.lab2.ex5;
import java.util.Scanner;
import java.util.Random;

public class BubbleSort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int[] a = new int[10];

        for( int i = 0 ; i < 10 ; i++)
            a[i] = random.nextInt(100);

        for( int i = 0 ; i < 9 ; i++ )
            for(int j = 0 ; j < 8 ; j++ )
                if( a[j] > a[j+1] ){
                    int swap = a[j];
                    a[j] = a[j+1];
                    a[j+1] = swap;
                }
        for( int i = 0 ; i < 10 ; i++)
            System.out.print(a[i] + " ");
    }
}
