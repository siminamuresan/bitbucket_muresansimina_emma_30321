package muresan.emma.simina.lab2.ex4;
import java.util.Scanner;

public class maxim {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        int a;

        int maximel = Integer.MIN_VALUE;

        for( int i = 0 ; i < N ; i++ ){
            a = scanner.nextInt();
            if( a > maximel ) maximel = a;
        }

        System.out.print("The maximum element is: " + maximel);
    }
}
