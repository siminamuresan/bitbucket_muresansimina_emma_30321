package muresan.emma.simina.lab2.ex7;
import java.util.Scanner;
import java.util.Random;

public class GuessTheNb {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int nb = random.nextInt(15);
        System.out.println("Insert a number between 0 and 14. ");

        boolean ok = false;
        int retries = 0;
        while(ok == false && retries <= 3 ){
            int x = scanner.nextInt();
            if( x == nb ){
                System.out.println("Congrats! Right Answear! ");
                ok = true;
                break;
            }
            else{
                if( x  > nb ) {
                    System.out.println("Wrong answer, your number it too high.Try again! ");
                    retries++;
                }
                else{
                    System.out.println("Wrong answer, your number it too low.Try again! ");
                    retries++;
                }

            }
        }

        if(retries > 3)
            System.out.println("You lost");

    }
}
