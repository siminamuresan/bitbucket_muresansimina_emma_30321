package Lab8.ex4;

import java.io.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ControlUnit {
    private static ControlUnit control;

    TempSensor t = new TempSensor();
    FireSensor bedroom = new FireSensor();
    FireSensor bathroom = new FireSensor();
    FireSensor kitchen = new FireSensor();

    CoolingUnit c = new CoolingUnit();
    HeatingUnit h = new HeatingUnit();

    AlarmUnit a = new AlarmUnit();
    GSMUnit g = new GSMUnit();

    private ControlUnit(){
    }

    public static ControlUnit getInstance(){
        if(control == null){
            control = new ControlUnit();
        }
        return control;
    }

    public void acting() throws IOException{
        BufferedWriter bw = null;

        try {
            bw = new BufferedWriter(new FileWriter("src/LAB8/ex4/system_logs.txt"));


        } catch (
                IOException ex) {
            System.out.println("File has not been found.");
            return;
        }
        if (t.getState() == -1){
            bw.write(c.toString() + '\n');
            t.setState(0);
        }
        else{
            bw.write(h.toString() + '\n');
            t.setState(0);
        }

        if(bedroom.isState()){
            bw.write(a.toString()+ '\n');
            bw.write(g.toString()+ '\n');
            bedroom.setState(false);
        }
        if(bathroom.isState()){
            bw.write(a.toString()+ '\n');
            bw.write(g.toString()+ '\n');
            bathroom.setState(false);
        }
        if(kitchen.isState()){
            bw.write(a.toString()+ '\n');
            bw.write(g.toString()+ '\n');
            bathroom.setState(false);
        }

        bw.close();
    }
}
