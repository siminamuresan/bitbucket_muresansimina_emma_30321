package Lab8.ex4;

public class TempSensor {
    private int state = 0;

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }
}
