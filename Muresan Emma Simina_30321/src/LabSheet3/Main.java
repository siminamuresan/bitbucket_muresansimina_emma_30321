package LabSheet3;

public class Main {

    public static void main(String[] args) {

        Rectangle a; //null
        Rectangle b = new Rectangle(); //default constructor

        b.length = 4;
        b.width = 3;

        b.PrintValues();

        int area = b.area();

        System.out.println("area = " +area);

        Rectangle c = new Rectangle();
        c.setValues(5,6);
    }

}
