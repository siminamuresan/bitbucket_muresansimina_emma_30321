package LabSheet3;

public class Flower {

    int petal;
    static int cont = 0;

    Flower(){
        cont++;
    }

    static public int numberOfConstructedObj(){
        return cont;
    }
    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }

        System.out.println("The number of registered flowers is: "+Flower.numberOfConstructedObj());
    }
}
