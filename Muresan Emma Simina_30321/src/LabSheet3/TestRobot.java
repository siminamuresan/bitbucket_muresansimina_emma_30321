package LabSheet3;

public class TestRobot {
    public static void main(String[] args) {

        Robot a = new Robot();

        System.out.println("The initial position : "+a.x);
        a.change(6);
        //System.out.println("New position : "+ a.toString()); //when printing the object, the toString is called automatically
        System.out.println(a);
    }
}
