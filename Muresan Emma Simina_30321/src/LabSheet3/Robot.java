package LabSheet3;


public class Robot {

    int x;

    Robot(){ x = 1;}

    public  void change(int k){
        if(k >= 1) this.x += k;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                '}';
    }
}
