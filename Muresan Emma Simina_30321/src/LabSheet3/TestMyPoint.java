package LabSheet3;
import java.util.Scanner;

public class TestMyPoint {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        MyPoint A = new MyPoint();
        MyPoint B = new MyPoint(2,5);

        System.out.println("The x coordinate of A : " +A.getX()+ " , the y coordinate : " +A.getY());
        System.out.println("Let's change them.");
        System.out.println("x : ");

        int x = scanner.nextInt();
        A.setX(x);

        System.out.println("y : ");

        int y = scanner.nextInt();
        A.setY(y);

        System.out.println("The new x coordinate of A : " +A.getX()+ " , the new y coordinate : " +A.getY());


        System.out.println("Now let's change B.");

        System.out.println("x : ");
        x = scanner.nextInt();

        System.out.println("y : ");
        y = scanner.nextInt();
        B.setXY(x,y);

        System.out.println("The new coordinates of B : " +B.toString());

        System.out.println("The distance between A and (7,3) : "+A.distance(7,3));
        System.out.println("The distance between A and B : "+A.distance(B));





    }
}
