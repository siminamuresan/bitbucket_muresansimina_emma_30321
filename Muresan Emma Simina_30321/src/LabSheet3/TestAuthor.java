package LabSheet3;
import java.util.Scanner;

public class TestAuthor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Author x = new Author("Leigh Bardugo","leigh.bardugo@outlook.com",'f');

        System.out.println("Author's name : " +x.getName());
        System.out.println("Author's gender : " +x.getGender());
        System.out.println("Author's email : " +x.getEmail());

        System.out.println("Do you want to change the email? Introduce it : ");

        String email_nou = scanner.nextLine();
        x.setEmail(email_nou);

        System.out.println(x.toString());
    }
}
