package LabSheet3;

public class ComplexNumber {
    int real, imaginary;

    public ComplexNumber(int r,int im){
        this.real = r;
        this.imaginary = im;
    }

    void ComplexAdd(ComplexNumber a){
        this.real += a.real;
        this.imaginary += a.imaginary;
    }
}
