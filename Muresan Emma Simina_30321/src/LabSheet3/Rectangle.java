package LabSheet3;

public class Rectangle {
    int length;
    int width;

    int area(){
        int a = length*width;
        return a;
    }

    void PrintValues(){
        System.out.println("The length = " +length+ ", the width = " +width);
    }

    void setValues(int l, int w){
        length = l;
        width = w;
    }
}
