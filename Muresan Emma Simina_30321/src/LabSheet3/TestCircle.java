package LabSheet3;

public class TestCircle {
    public static void main(String[] args) {
        Circle a = new Circle();
        Circle b = new Circle(2.3);
        Circle c = new Circle(1.2,"blue");

        System.out.println("Circle a has the radius = " + a.getRadius()+ " and the area = "+a.getArea());
        System.out.println("Circle b has the radius = " + b.getRadius()+ " and the area = "+b.getArea());
        System.out.println("Circle c has the radius = " + c.getRadius()+ " and the area = "+c.getArea());

    }
}
