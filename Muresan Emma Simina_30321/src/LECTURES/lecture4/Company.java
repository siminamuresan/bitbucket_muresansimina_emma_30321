package LECTURES.lecture4;

public class Company extends Customer{
    private String name;
    private String regNum;

    Company(String address, String phone, String name,String regNum){
        super(address,phone);
        this.name = name;
        this.regNum = regNum;
    }
}
