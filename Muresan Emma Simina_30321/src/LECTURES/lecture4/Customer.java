package LECTURES.lecture4;

import java.sql.SQLOutput;

public class Customer {
    private String address;
    private String phone;
    private ShoppingCart shoppingCart = new ShoppingCart();

    public Customer(String addres, String phone){
        this.address = address;
        this.phone = phone;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public void showDetails(){
        System.out.println(this);
    }
}
