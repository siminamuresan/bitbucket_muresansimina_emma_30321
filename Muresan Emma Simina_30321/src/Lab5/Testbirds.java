package Lab5;

public class Testbirds {
    public static void main(String[] args) {
        Bird b1 = new Penguin();
        Bird b2 = new Goose();

        b1.move(); // swimming
        b2.move(); //floating
    }
}
