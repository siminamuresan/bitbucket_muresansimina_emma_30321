package Lab5.Homework.Ex3;

public class ProxyImage implements Image {
    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
            this.RotatedImage();
        }
        else
            realImage.display();
    }

    @Override
    public void RotatedImage() {
        System.out.println("Display rotated " +this.fileName);
    }
}
