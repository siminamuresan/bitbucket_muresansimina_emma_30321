package Lab5.Homework.Ex3;

import Lab5.Homework.Ex3.Image;

public class RealImage implements Image {
    private String fileName;

    public RealImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Displaying " + fileName);
    }

    @Override
    public void RotatedImage() {
        System.out.println("Display rotated " +fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}

////////RotatedImage not implemented
