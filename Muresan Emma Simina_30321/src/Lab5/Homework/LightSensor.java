package Lab5.Homework;

import java.util.Random;

public class LightSensor extends Sensor{
    Random random = new Random();

    @Override
    public int readValue() {
        return random.nextInt(100);
    }

    public void lightSensor(){
        System.out.println("Value of light sensor : " +this.readValue());
    }
}
