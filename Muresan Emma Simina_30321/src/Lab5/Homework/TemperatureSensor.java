package Lab5.Homework;
import java.util.Random;

public class TemperatureSensor extends Sensor {
    Random random = new Random();

    @Override
    public int readValue() {
        return random.nextInt(100);
    }

    public void tempSensor(){
        System.out.println("Value of temp. sensor : " +this.readValue());
    }
}
