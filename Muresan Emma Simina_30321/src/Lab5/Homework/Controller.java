package Lab5.Homework;

import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    Timer timer;
    LightSensor L = new LightSensor();
    TemperatureSensor T = new TemperatureSensor();

    public void control(){
        timer = new Timer();
        timer.schedule(new RemindTask(),
                       0,        //initial delay
                       1*1000);  //subsequent rate
        }

    class RemindTask extends TimerTask{
        int num = 20;
        public void run() {
            if (num > 0) {
                L.lightSensor();
                T.tempSensor();
                System.out.println("Beep!");
                num--;
            } else {
                System.out.println("Time's up!");
                System.exit(0);
            }
        }
    }
}


