package Lab4;

public class Circle {
    private double radius;
    private String color;

    Circle(){
        this.radius = 1.0f;
        this.color = "red";
    }

    Circle(double radius){
        this.radius = radius;
        this.color = "red";
    }

    Circle(double radius, String color){
        this.radius = radius;
        this.color = color;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        return Math.PI*Math.pow(this.radius,2);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}
