package Lab4;
import java.util.Scanner;

public class TestBookex4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Author[] a = new Author[3];

        a[0] = new Author("Leigh Bardugo","leigh.bardugo@gmail.com",'f');
        a[1] = new Author("Tahereh Mafi","taherehmafi2022@yahoo.com",'f');
        a[2] = new Author("Jennifer L. Armentrout", "jennifer_l_armentrout@outlook.com",'f');
        Bookex4 a1 = new Bookex4("Shadow & Bone",a, 37.68);
        Bookex4 b1 = new Bookex4("Shatter Me",a,45.25,73);

        System.out.println("The details of the first book: ");
        System.out.println("Title : " + a1.getName());
        System.out.println("Author's name : " );
        for( int i = 0 ; i < a1.getAuthors().length ; i++){
            System.out.println(a[i].getName());
        }
        System.out.println("Author's gender : " );
        for( int i = 0 ; i < a1.getAuthors().length ; i++){
            System.out.println(a[i].getGender());
        }
        System.out.println("Author's email : ");
        for( int i = 0 ; i < a1.getAuthors().length ; i++){
            System.out.println(a[i].getEmail());
        }
        System.out.println("Price : " + a1.getPrice());

        System.out.println("Let's change the price and the quantity .");

        System.out.println("Before the changes : ");

        System.out.println("The details of the second book: ");
        System.out.println("Title : " + b1.getName());
        System.out.println("Author's name : " );
        for( int i = 0 ; i < b1.getAuthors().length ; i++){
            System.out.println(a[i].getName());
        }
        System.out.println("Author's gender : " );
        for( int i = 0 ; i < b1.getAuthors().length ; i++){
            System.out.println(a[i].getGender());
        }
        System.out.println("Author's email : ");
        for( int i = 0 ; i < b1.getAuthors().length ; i++){
            System.out.println(a[i].getEmail());
        }
        System.out.println("Price : " + b1.getPrice());


        System.out.println("Now, enter the price : ");

        b1.setPrice(scanner.nextDouble());

        System.out.println("Enter the quantity : ");

        b1.setQtyInStock(scanner.nextInt());


        System.out.println("The new item is : ");
        System.out.println(b1 + " the price : " + b1.getPrice() + "; the quantity : " + b1.getQtyInStock());

        System.out.println("The names of registered authors : " );
        a1.printAuthors();

    }
}
