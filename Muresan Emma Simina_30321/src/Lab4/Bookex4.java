package Lab4;


public class Bookex4 {
    private String name;
    private Author[] authors ;
    private double price;
    private int qtyInStock = 0;

    Bookex4(String name,Author[] authors,double price){
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    Bookex4(String name, Author[] authors,double price,int qtyInStock){
            this.name = name;
            this.authors = authors;
            this.price = price;
            this.qtyInStock = qtyInStock;
        }

        public String getName() {
                return name;
            }

    public Author[] getAuthors(){
        return this.authors;
    }



    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return  name + " by " +
                authors.length + " authors.";
    }

    public void printAuthors() {
        for(int i = 0; i < authors.length; i++)
            System.out.println(authors[i].getName());
    }
}
