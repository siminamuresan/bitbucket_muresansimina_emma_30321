package Lab4;

public class Cylinder extends Circle{
    private double height = 1.0;

    Cylinder(){
        super(); //must call super to initialize the inherited attributes
    };

    Cylinder(double radius){
        super(radius);
    }

    Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume(){
        return this.height*super.getArea();
    }
    @Override
    public double getArea() {return 2*super.getArea()+2*Math.PI*super.getRadius()*this.height;}

    /// in the exercise is said that i have to overwrite the getArea function
    /// but I don't understand why. In the cylinder we need that area of the circle and the
    /// height for the volume. So what's wrong with the getArea function?
    /////////////////Because the area of the cylinder is the area of two circle and the tube A=2πrh+2πr2 to return the cylinder area
}
