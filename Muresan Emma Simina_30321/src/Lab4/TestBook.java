package Lab4;
import java.util.Scanner;

public class TestBook {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Author a = new Author("Leigh Bardugo","leigh.bardugo@gmail.com",'f');
        Author b = new Author("Tahereh Mafi","taherehmafi2022@yahoo.com",'f');
        Book a1 = new Book("Shadow & Bone", a, 37.68);
        Book b1 = new Book("Shatter Me",b,45.25,73);

        System.out.println("The details of the first book: ");
        System.out.println("Title : " + a1.getName());
        System.out.println("Author's name : " + (a1.getAuthor()).getName());
        System.out.println("Author's gender : " + (a1.getAuthor()).getGender());
        System.out.println("Author's email : " + (a1.getAuthor()).getEmail());
        System.out.println("Price : " + a1.getPrice());

        System.out.println("Let's change the price, the quantity and the author's email of the second book.");

        System.out.println("Before the changes : ");

        System.out.println("The details of the second book: ");
        System.out.println("Title : " + b1.getName());
        System.out.println("Author's name : " + (b1.getAuthor()).getName());
        System.out.println("Author's gender : " + (b1.getAuthor()).getGender());
        System.out.println("Author's email : " + (b1.getAuthor()).getEmail());
        System.out.println("Price : " + b1.getPrice());


        System.out.println("Now, enter the price : ");

        b1.setPrice(scanner.nextDouble());

        System.out.println("Enter the quantity : ");

        b1.setQtyInStock(scanner.nextInt());

        System.out.println("Enter the new email : ");

        (b1.getAuthor()).setEmail(scanner.nextLine()); /// I don't understand why it doesn't work

        System.out.println("The new item is : ");
        System.out.println(b1 + " the price : " + b1.getPrice() + "; the quantity : " + b1.getQtyInStock());
    }
}
