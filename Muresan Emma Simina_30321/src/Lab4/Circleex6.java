package Lab4;

public class Circleex6 extends Shape{
    private double radius;

    Circleex6(){
        super();
        this.radius = 1.0;
    }

    Circleex6(double radius){
        super();
        this.radius = radius;
    }

    Circleex6(double radius, String color, boolean filled){
        super(color,filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI*Math.pow(this.radius,2);
    }

    public double getPerimeter() {return 2*Math.PI*this.radius;}

    @Override
    public String toString() {
        return "A circle with radius=" +
                 radius + ", which is a subclass of " +
                super.toString();
    }
}
