package Lab4;


public class TestShape {
    public static void main(String[] args){
        Shape shape = new Shape("blue",false);

        System.out.println("The color is : " + shape.getColor());
        shape.setColor("purple");
        System.out.println("Is filled? " +shape.isFilled());
        shape.setFilled(true);
        System.out.println("After changes : " +shape);

        Circleex6 c1 = new Circleex6();
        Circleex6 c2 = new Circleex6(2.6);
        Circleex6 c3 = new Circleex6(3.1,"orange", true );

        System.out.println("The first circle : " + c1);
        System.out.println("For the second circle we have the radius = " +c2.getRadius());
        c3.setRadius(6.1);
        System.out.println("Now the radius is " + c3.getRadius());
        System.out.println("The area of c2 is " + c2.getArea() + " and the perimeter " +c2.getPerimeter());

        Rectangle r1 = new Rectangle();
        System.out.println("The rectangle's width is : " + r1.getWidth());
        System.out.println("The rectangle's length is : " + r1.getLength());
        System.out.println("Area : " + r1.getArea());
        System.out.println("Perimeter : " +r1.getPerimeter());

        Square s1 = new Square();
        Square s2 = new Square(1.4);
        Square s3 = new Square(2.5,"black",true);
        System.out.println("Now squares. The first side is " + s1.getSide());
        s1.setSide(3.2);
        System.out.println("Now the side is " +s1.getSide());

        System.out.println("2nd square has a side of " +s2.getSide());
        System.out.println("We use the setLength and setWidth methods." );
        s3.setLength(4.6);
        System.out.println("First one : " + s3);
        s3.setWidth(1.9);
        System.out.println("The second one : " + s3);

    }


}
