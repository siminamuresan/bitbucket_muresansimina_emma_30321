package lecture6;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random;

public class Win extends JFrame implements ActionListener {
    Win(int x,int y){
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(10,10,400,100);
        JButton b1 = new JButton("Click to close the window");

        //b1.addActionListener(); ///polymorfismn
        this.add(b1);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        int x = new Random().nextInt(1800);
        int y = new Random().nextInt(800);
        new Win(x,y);
    }

    @Override
    public void setDefaultCloseOperation(int operation) {
        super.setDefaultCloseOperation(operation);
    }
}