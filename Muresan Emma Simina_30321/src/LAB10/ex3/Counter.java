package LAB10.ex3;

public class Counter extends Thread{
    private int start;
    private int finish;

    Counter(int start,int finish){
        this.start = start;
        this.finish = finish;
    }

    public void count() {
        for(int i = start; i <= finish; i++) System.out.println("step = " +i);
    }

    public void run(){
        for(int i = start ; i<=finish;i++){
            System.out.println("step= " +i);
            try{
                Thread.sleep((int)(Math.random()*1000));
            } catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Counter c1 = new Counter(0,100);
        Counter c2 = new Counter(100,200);

        c1.run();
        c2.run();
    }
}
