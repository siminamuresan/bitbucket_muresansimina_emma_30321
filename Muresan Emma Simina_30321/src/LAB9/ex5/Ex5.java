package LAB9.ex5;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Ex5 extends JFrame {

    JLabel insert, insName, insStation, insSegment;
    JLabel[] station;
    JLabel[][] segment, train;
    JTextField tName, tSt, tSeg;
    JTextField[] tstation;
    JTextField[][] ttrain;
    JButton buton;

    Ex5() {
        setTitle("sistem statii");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;
        int i = 0, j = 0;

        station = new JLabel[3];
        station[0] = new JLabel("Station");
        station[0].setBounds(10, 50, width, height);
        add(station[0]);

        tstation = new JTextField[3];
        tstation[0] = new JTextField("Cluj-Napoca");
        tstation[0].setBounds(55, 50, width, height);
        tstation[0].setEditable(false);
        add(tstation[0]);

        station[1] = new JLabel("Station");
        station[1].setBounds(155, 50, width, height);
        add(station[1]);

        tstation[1] = new JTextField("Bucuresti");
        tstation[1].setBounds(200, 50, width, height);
        tstation[1].setEditable(false);
        add(tstation[1]);

        station[2] = new JLabel("Station");
        station[2].setBounds(300, 50, width, height);
        add(station[2]);

        tstation[2] = new JTextField("Timisoara");
        tstation[2].setBounds(345, 50, width, height);
        tstation[2].setEditable(false);
        add(tstation[2]);

        segment = new JLabel[3][3];
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                segment[i][j] = new JLabel("segment " + (i + 1));
                segment[i][j].setBounds(10 + j * 145, 100 + i * 50, width, height);
                add(segment[i][j]);
            }
        }

        train = new JLabel[3][3];
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                train[i][j] = new JLabel("train");
                train[i][j].setBounds(75 + j * 145, 100 + i * 50, width, height);
                add(train[i][j]);
            }
        }

        ttrain = new JTextField[3][3];
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                ttrain[i][j] = new JTextField("-");
                ttrain[i][j].setBounds(105 + j * 145, 100 + i * 50, 40, height);
                ttrain[i][j].setEditable(false);
                add(ttrain[i][j]);
            }
        }

        addTrain("Vrum", 1, 3);
        addTrain("1234", 2, 2);

        insert = new JLabel("Insert a train!");
        insert.setBounds(10, 250, width, height);
        add(insert);

        insName = new JLabel("name");
        insName.setBounds(10, 270, width, height);
        add(insName);

        tName = new JTextField("");
        tName.setBounds(45, 270, 40, height);
        add(tName);

        insStation = new JLabel("station");
        insStation.setBounds(95, 270, width, height);
        add(insStation);

        tSt = new JTextField("");
        tSt.setBounds(140, 270, 40, height);
        add(tSt);

        insSegment = new JLabel("segment");
        insSegment.setBounds(190, 270, width, height);
        add(insSegment);

        tSeg = new JTextField("");
        tSeg.setBounds(245, 270, 40, height);
        add(tSeg);

        buton = new JButton("Insert");
        buton.setBounds(300, 270, width, height);
        buton.addActionListener(new TratareButon());
        add(buton);
    }

    public void addTrain(String name, int station, int segment) {
        ttrain[segment - 1][station - 1].setText(name);
    }



    public static void main(String[] args) {
        Ex5 e = new Ex5();
    }

    class TratareButon implements ActionListener{
        public void actionPerformed(ActionEvent e) {

            String train = Ex5.this.tName.getText();
            int segment = Integer.parseInt(Ex5.this.tSeg.getText());
            int station = Integer.parseInt(Ex5.this.tSt.getText());

            if(ttrain[segment-1][station-1].getText().equals("-"))

                addTrain(train, station, segment);
            else System.out.println("Can not pun a train on this segment");
        }
    }
}
