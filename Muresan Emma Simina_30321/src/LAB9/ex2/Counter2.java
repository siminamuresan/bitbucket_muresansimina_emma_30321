package LAB9.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class Counter2 extends JFrame{

    JTextField counter = new JTextField();
    JButton button = new JButton("PUSH to increment");

    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            cont++;
            Counter2.this.counter.setText(String.valueOf(cont));
        }
    };

    int cont = 0;

    public Counter2(){
        setTitle("INCREMENT ON PUSHING");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,200);
        setLocation(600,300);
        setVisible(true);


    }

    public void init(){
        this.setLayout(null);
        int width = 200;
        int height = 20;

        counter.setBounds(90,50,width,height);
        add(counter);
        Counter2.this.counter.setText(String.valueOf(cont));

        button.setBounds(90,100,width,height);
        add(button);
        button.addActionListener(listener);

    }

    public static void main(String[] args) { new Counter2(); }

}
