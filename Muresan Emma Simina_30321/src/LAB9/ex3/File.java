package LAB9.ex3;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileReader;

public class File extends JFrame{
    JTextArea area = new JTextArea();
    JTextField text = new JTextField();
    JButton button = new JButton("OPEN FILE");
    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                String name = File.this.text.getText();
                open(name);
            }catch(Exception ignored){}
        }
    };

    File(){
        setTitle("PRESS Button to OPEN FILE");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,200);
        setLocation(600,300);
        setVisible(true);

        this.setLayout(null);
        int width = 100;
        int height = 20;

        text.setBounds(50,50,width,height);
        add(text);

        button.setBounds(50,100,width,height);
        add(button);
        button.addActionListener(listener);

        area.setBounds(200,50,150,80);
        area.setLineWrap(true);
        add(area);
    }

    void open(String file){
        try{
            area.setText("");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String a = "";
            a = br.readLine();
            while(a!=null){
                area.append(a+"\n");
                a = br.readLine();
            }
            br.close();
        }catch(Exception e){
            System.out.println("File could not be opened!");
        }

    }

    public static void main(String[] args) {
        new File();
    }
}
