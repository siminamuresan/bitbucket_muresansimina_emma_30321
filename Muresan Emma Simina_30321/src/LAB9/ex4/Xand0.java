package LAB9.ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class Xand0 extends JFrame{

    private boolean turnX = true;
    JButton[][] buttons = new JButton[3][3];
    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton clicked = (JButton) e.getSource();

            if(Objects.equals(clicked.getText(),"")){
                if(turnX){
                    clicked.setText("X");
                    turnX = false;
                }
                else{
                    clicked.setText("O");
                    turnX = true;
                }

                String winner = Winner();
                if(!Objects.equals(winner,"None")){
                    displayWinner(winner);
                }
            }
        }
    };

    Xand0(){
        setTitle("X&0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        setLocation(600,200);
        setVisible(true);

        this.setLayout(null);

        for(int i = 0 ; i < 3; i++ )
            for(int j = 0 ; j < 3 ; j++ ){
                buttons[i][j] = new JButton("");
                buttons[i][j].setBounds(40+i*100,20+j*100,100,100);
                buttons[i][j].addActionListener(listener);
                add(buttons[i][j]);
            }
    }

    private String Winner(){
        for(int i = 0; i < 3; i++ )
            if(Objects.equals(buttons[i][0].getText(),buttons[i][1].getText()) && Objects.equals(buttons[i][1].getText(),buttons[i][2].getText())  && !buttons[i][1].getText().isEmpty()
                    || Objects.equals(buttons[0][i].getText(), buttons[1][i].getText()) && Objects.equals(buttons[1][0].getText(), buttons[2][i].getText()) && !buttons[1][0].getText().isEmpty() ){
                return buttons[i][i].getText();
            }
        if(Objects.equals(buttons[0][0].getText(), buttons[1][1].getText()) && Objects.equals(buttons[1][1].getText(), buttons[2][2].getText()) && !buttons[1][1].getText().isEmpty()
                || Objects.equals(buttons[0][2].getText(), buttons[1][1].getText()) && Objects.equals(buttons[1][1].getText(), buttons[2][0].getText()) && !buttons[1][1].getText().isEmpty() )
        {
            return buttons[1][1].getText();
        }

        return "None";
    }

    private void displayWinner(String w){
        JTextField text = new JTextField();
        text.setText("The Winner is " + w);
        text.setBounds(100,350,200,50);
        add(text);
    }

    public static void main(String[] args) {
        Xand0 joc = new Xand0();
    }
}
