package LabSheet67;

import java.util.Scanner;

public class ConsoleMenu {
    public static void interactive(Dictionary d){

        Scanner scanner=new Scanner(System.in);

        boolean ok =true;

        while (ok){
            System.out.println("Select an option!");
            System.out.println("1. Search for a word");
            System.out.println("2. Add a new word");
            System.out.println("3. List all words");
            System.out.println("4. Get all definitions");
            System.out.println("0. Exit");

            System.out.println("Your option :");
            int option = scanner.nextInt();
            scanner.skip("\n");

            switch (option){
                case 0:
                    ok = false;
                    break;
                case 1: {
                    System.out.print("Type the word for searching: ");
                    String word = scanner.nextLine();
                    Definition def = d.getDefinition(new Word(word));
                    if (def == null)
                        System.out.println("Word " + word + " not in dictionary");
                    else
                        System.out.println(def.getDescription());
                    break;
                }
                case 2: {
                    System.out.print("Word to add: ");
                    String word = scanner.nextLine();
                    System.out.print("Definition for " + word + ": ");
                    String def = scanner.nextLine();
                    d.addWord(new Word(word), new Definition(def));
                    break;
                }
                case 3: {
                    System.out.println("All words: ");
                    for(Word w : d.getAllWords())
                        System.out.print(w.getName() + ", ");
                    System.out.println();
                    break;
                }
                case 4:{
                    System.out.println("All definitions:");
                    for(Definition def: d.getAllDefinitions())
                        System.out.println(def.getDescription()+",");
                    System.out.println();
                    break;
                }
                default :
                    System.out.println("Invalid!");
                    break;

            }
        }
    }
}
