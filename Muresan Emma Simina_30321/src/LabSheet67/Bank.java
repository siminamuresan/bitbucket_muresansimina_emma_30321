package LabSheet67;

import java.util.ArrayList;

public class Bank {
    private ArrayList<BankAccount> array= new ArrayList<BankAccount>();
    public void addAccount(String owner, double balance){
        BankAccount b = new BankAccount(owner,balance);
        array.add(b);
    }


    public void printAccountssorted() {
        BankAccount aux = new BankAccount();
        for (int i = 0; i < array.size() - 1; i++) {
            for (int j = i + 1; j < array.size(); j++) {
                if (array.get(i).getBalance() > array.get(j).getBalance()) { /// the equals method compare also the owners name
                    aux = array.get(i);
                    array.set(i, array.get(j));
                    array.set(j, aux);
                }
            }
        }

        for (BankAccount i : array) {
            System.out.println(i.getOwner() + " " +i.getBalance());
        }
    }

    public void printAccountsrange( double min, double max){
        for (BankAccount i : array) {
            if(i.getBalance() >= min && i.getBalance() <= max) System.out.println(i.getOwner() + " " +i.getBalance());
        }
    }


    public void sortOwner(){
        BankAccount aux = new BankAccount();
        for (int i = 0; i < array.size() - 1; i++) {
            for (int j = i + 1; j < array.size(); j++) {
                if (array.get(i).getOwner().compareTo(array.get(j).getOwner()) >0) {
                    aux = array.get(i);
                    array.set(i, array.get(j));
                    array.set(j, aux);
                }
            }
        }

        for (BankAccount i : array) {
            System.out.println(i.getOwner() + " " +i.getBalance());
        }
    }
}
