package LabSheet67;

public class TestDictionary {
    public static void main(String[] args) {
        Dictionary d = new Dictionary();

        d.addWord(new Word("tea"),new Definition("Good for a cold."));
        d.addWord(new Word("timetravel"),new Definition("This would be really nice."));
        d.addWord(new Word("Universe"),new Definition("Everything surrounding us."));
        d.addWord(new Word("pizza"),new Definition("We all need it sometimes even if we don't want to."));

        ConsoleMenu.interactive(d);
    }
}
