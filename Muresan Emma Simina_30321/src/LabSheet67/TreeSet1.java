package LabSheet67;

import  java.util.TreeSet;
import java.io.*;
import java.util.*;

public class TreeSet1 {
    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        BankTreeSet bank = new BankTreeSet();

        double min = 0.0, max = java.lang.Long.MAX_VALUE;

        bank.addAccount("Pop Aurel", 23569.45);
        bank.addAccount("Rus Sebastian", 101456.95);
        bank.addAccount("Silaghi Mirela", 124567.23);
        bank.addAccount("Melany Owens", 1234.34);
        bank.addAccount("Dragon Andrei", 567890.34);
        bank.addAccount("Dragon Andrei", 567890.34);
        bank.addAccount("Kate Evans", 10156.95);
        bank.addAccount("Christian Maxwell", 300567.23);
        bank.addAccount("Venus DeLaCroix", 101345.45);


        System.out.println("The array of accounts with balance in a given range. ");
        System.out.println("Insert de minmum range : ");
        min = scanner.nextInt();
        System.out.println("Insert the maximum range : ");
        max = scanner.nextInt();

       bank.printAccountsrange(min, max);

    }
}
