package LabSheet67;

import  java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Dictionary {
    private HashMap<Word,Definition> dictionary = new HashMap<>();

    public void addWord(Word w, Definition d){
        dictionary.put(w,d);
    }

   public Definition getDefinition(Word w){
        return dictionary.get(w);
   }

    public List<Word> getAllWords(){
        return dictionary.keySet().stream().collect(Collectors.toList());
    }

    public List<Definition> getAllDefinitions(){
        return dictionary.values().stream().collect(Collectors.toList());
    }
}
