package LabSheet67;
//you should add the other methods, printAccounts, and  getAccount
import java.util.TreeSet;
import java.lang.Comparable;

public class BankTreeSet {
    private TreeSet<BankAccount> array= new TreeSet<>();

    public void addAccount(String owner, double balance){
        BankAccount b = new BankAccount(owner,balance);
        array.add(b);
    }


    public void printAccountsrange( double min, double max){
        for (BankAccount i : array) {
            if(i.getBalance() >= min && i.getBalance() <= max) System.out.println(i.getOwner() + " " +i.getBalance());
        }
    }

   

}
