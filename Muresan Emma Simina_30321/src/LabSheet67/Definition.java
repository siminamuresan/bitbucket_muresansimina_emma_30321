package LabSheet67;

public class Definition {
    private String description;

    Definition() {this.description = "";}
    Definition(String description) {this.description = description;}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
