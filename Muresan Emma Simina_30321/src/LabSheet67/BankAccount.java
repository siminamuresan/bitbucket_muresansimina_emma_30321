package LabSheet67;

import LabSheet3.ComplexNumber;

import java.util.Objects;

public class BankAccount /*implements Comparable*/{
    private String owner;
    private double balance;

    BankAccount(){this.owner = ""; this.balance = 0;}
    BankAccount(String owner) { this.owner = owner; this.balance = 0;}
    BankAccount(String owner,double balance) {this.owner = owner; this.balance = balance;}

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void withdraw(double amount){}
    public void deposit(double amount){}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Pop Aurel",23569.45);
        BankAccount b2 = new BankAccount("Pop Aurel",23569.45);
        BankAccount b3 = new BankAccount("Silaghi Mirela",124567.23);
        ComplexNumber c1 = new ComplexNumber(12,2);

        System.out.println("b1 ? b2 " +b1.equals(b2));
        System.out.println("b1 ? b3 " +b1.equals(b3));
        System.out.println("b1 ? c1 " +b1.equals(c1));
        b1.hashCode();
    }
/*
    @Override
    public int compareTo(Object o) {
        if(o instanceof BankAccount ba) {
            return Double.compare(this.balance, ba.balance);
        }
        return -1;
    }*/
}
