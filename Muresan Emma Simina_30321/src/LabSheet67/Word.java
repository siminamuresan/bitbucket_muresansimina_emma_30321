package LabSheet67;
//implement the equals and hashcode methods
public class Word {
    private String name;

    Word(){this.name = "";}
    Word(String name){this.name = name;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
