package LAB7.ex4;
//use object output stream to write the object to a file
//object input stream to read them from the file
import java.io.*;
import java.util.Scanner;

//// Sorry for this.
/// I don t understand something. If you can tell me what's wrong
// //it would mean a lot to me
/// So if I press repeatedly 1 and add some cars they will be saved
/// because if i close the program the data will appear in the text file.
/// But if then I choose option 2 - display detail of a given car( searching by model)
/// it won't display anything and the text file will be erased
/// for the 3 option- display all cars -> the same
public class Console {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        BufferedWriter bw = null;
        BufferedReader br = null;

        Scanner s = new Scanner(System.in);
        int choice = -1;
        boolean ok = true;

        System.out.println("Hello! Welcome in our application! What do you want to do?");
        System.out.println("Press the key indicated for each request.");

        try {
            bw = new BufferedWriter(new FileWriter("src/LAB7/ex4/data.txt"));


        } catch (IOException ex) {
            System.out.println("File has not been found.");
            return;
        }

        try {
            br = new BufferedReader(new FileReader("src/LAB7/ex4/data.txt"));


        } catch (IOException ex) {
            System.out.println("File has not been found.");
            return;
        }



        while (ok) {
            System.out.println("PRESS");
            System.out.println("1 - to CREATE a new Car and SAVE it");
            System.out.println("2 - to READ a given Car - by model");
            System.out.println("3 - to VIEW existing Cars");
            System.out.println("0 - EXIT!");

            choice = s.nextInt();

            switch (choice) {
                case 1:
                    menuwrite(bw); // write cars
                    break;
                case 2:
                    readCar(br); /// find a car
                    break;
                case 3:
                    readAllCars(br);  /// display all cars
                    break;
                default:
                    ok = false;
                    System.out.println("EXITING!");
                    break;
            }
        }
        bw.close();
        br.close();


    }

    public static void menuwrite(BufferedWriter bw) throws IOException {

        Scanner scan = new Scanner(System.in);
        String model;
        double price;

        System.out.println("Enter the model : ");
        model = scan.nextLine();
        System.out.println("Enter the price : ");
        price = scan.nextDouble();

        Car car = new Car(model, price);
        car.writeFile(bw);

    }


    public static void readCar(BufferedReader br)throws IOException {

        String model;
        Scanner s = new Scanner(System.in);
        System.out.println("Enter the model : ");
        model = s.nextLine();
        BufferedReader in = new BufferedReader(
                new FileReader("D://Anul 2//Sem 2//Software engineering//Laboratories//Bitbucket//Muresan Emma Simina_30321//src//LAB7//ex4//data.txt"));
        String line , line1= new String();
//D:\Anul 2\Sem 2\Software engineering\Laboratories\Bitbucket\Muresan Emma Simina_30321\src\LAB7\ex4//data.txt

        while ((line = in.readLine())!=null) { /// untill the end of the text file line by line
            line1 += line + "\n";
            int i = 0;
            boolean ok = false; /// suppose there is not the model looked for

            while(i < line1.length() && !ok) {
                int j = i;

                StringBuilder aux = null;
                while (line1.charAt(j) != ' ') { // each word taken to pe compared with given model
                    aux.append(line1.charAt(j));
                    j++;
                }

                if ((aux.toString()).equals(model)) { /// found the model name
                        System.out.println(line1); // print it
                        System.out.print("\r\n");
                        ok = true;
                    }
                else i = j+1; /// if the word not equal with the model name we move forward on the line
            }
            line = in.readLine(); /// read next line
        }


    }

    public static void readAllCars(BufferedReader br)throws IOException{

        String line = br.readLine();
        while(line!=null){
            System.out.println(line);
            line = br.readLine();
        }
    }
}