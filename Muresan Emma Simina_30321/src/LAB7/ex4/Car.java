package LAB7.ex4;

import java.io.*;

public class Car implements Serializable {
    private String model;
    private double price;

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }


    @Override
    public String toString() {
        return "Car " +
                "model = " + model +
                " , price=" + price +
                "\r\n";
    }

    public void writeFile(BufferedWriter bw) throws IOException {
        String s;
        s = this.toString();
        bw.write(s);
    }
}





