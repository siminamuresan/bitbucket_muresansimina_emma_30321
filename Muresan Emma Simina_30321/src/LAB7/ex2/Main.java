package LAB7.ex2;

import java.io.File;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws Exception {
        File file = new File("D:\\Anul 2\\Sem 2\\Software engineering\\Laboratories\\Bitbucket\\Muresan Emma Simina_30321\\src\\LAB7\\ex2\\data");
        Scanner scanner = new Scanner(System.in);
        char n;
        int cont = 0;

        System.out.println("Enter the character which you want to be counted.");
        n = scanner.next().charAt(0);

        try (FileReader fr = new FileReader(file))
        {
            int content;
            while ((content = fr.read()) != -1) {
                if(n == (char) content) cont++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("The number of "+n+ " is: "+cont);
    }
}
