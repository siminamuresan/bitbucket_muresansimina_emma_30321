package LAB7.ex4withoutconsole;

import java.io.*;

public class Car  {
    private String model;
    private int price;

    public Car(String model,int price){
        this.price=price;
        this.model=model;

    }

    public int getPrice() {
        return price;
    }

    public String getModel() {
        return model;
    }

    public void writeToFile( ) throws IOException {
        BufferedWriter bw;
        try{
            bw=new BufferedWriter(new FileWriter("src/LAB7/ex4withoutconsole/data.txt"));
        }catch(IOException e){
            System.out.println("Problems with creating file");
            return;
        }
        String s= model+ " "+ price+ "/n" ;
        bw.write(s);
        bw.close();
    }

}
