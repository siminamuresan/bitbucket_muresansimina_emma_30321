package LAB7.ex4withoutconsole;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Console {
    public static void main(String[] args) throws IOException {
        Scanner sc=new Scanner(System.in);
        int  choice=1;
        while(choice!=0){
            System.out.println("If you want to add a car press 1 if you wat to see all the cars press 2 else press 0");
            choice=sc.nextInt();
            switch (choice){
                case 1:
                    System.out.println("model:");
                    String model=sc.next();
                    System.out.println("price:");
                    int price=sc.nextInt();
                    Car cr=new Car(model,price);
                    cr.writeToFile();
                    break;
                case 2:
                    showFile();
                    break;

            }

        }
    }
    public static void showFile() throws IOException {
        BufferedReader br;
        try{
            br= new BufferedReader(new FileReader("src/Lab7/data.txt"));
        }catch (IOException e){
            System.out.println("File not found!");
            return;

        }
        String s;
        while ((s = br.readLine()) != null){
            System.out.println(s);
            System.out.println();
        }
        br.close();
    }
}