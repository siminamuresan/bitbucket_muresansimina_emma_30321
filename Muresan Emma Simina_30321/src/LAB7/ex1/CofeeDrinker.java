package LAB7.ex1;

public class CofeeDrinker {
    void drinkCofee(Cofee c, int nb) throws TemperatureException, ConcentrationException, MaximumNbException {
        if(c.getIndex() == nb)
            throw  new MaximumNbException(nb,"You reached the maximum number of coffees for this machine! This is it : ");
        else{
            System.out.println("Make a coffe");
            if(c.getTemp()>60)
                throw new TemperatureException(c.getTemp(),"Coffee is too hot!");
            if(c.getConc()>50)
                throw new ConcentrationException(c.getConc(),"Coffee concentration too high!");
        }
        System.out.println("Drink coffee:"+c);
    }
}
