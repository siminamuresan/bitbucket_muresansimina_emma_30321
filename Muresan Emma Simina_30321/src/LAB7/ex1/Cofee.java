package LAB7.ex1;

public class Cofee {
    private int temp;
    private int conc;
    private int index;

    Cofee(int t,int c,int i){temp = t;conc = c;index = i;}
    int getTemp(){return temp;}
    int getConc(){return conc;}

    public int getIndex() {
        return index;
    }

    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+":index="+index+"]";}

}
