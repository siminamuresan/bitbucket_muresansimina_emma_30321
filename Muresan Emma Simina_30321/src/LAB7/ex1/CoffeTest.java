package LAB7.ex1;

import java.util.Scanner;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();
        Scanner s = new Scanner(System.in);

        boolean ok = true;
        int choice = -1;
        int max = 0;
        int cont = 0;

        System.out.println("Enter the maximum number of coffees this machine can make : ");
        max = s.nextInt();

        while(ok){
            System.out.println("For a new coffee press 1");
            System.out.println("Otherwise press 0");

            choice = s.nextInt();
            switch(choice) {
                case 1: {
                    cont++;
                    Cofee c = mk.makeCofee(cont);
                    try {
                        d.drinkCofee(c,max+1);
                    } catch (MaximumNbException e) {
                            System.out.println("Exception:" + e.getMessage()  +e.getNb());
                            ok = false;
                    } catch (ConcentrationException e) {
                        System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc() + " and index= "+c.getIndex());
                    } catch (TemperatureException e) {
                            System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp() + " and index= "+c.getIndex());
                    } finally {
                        System.out.println("Throw the cofee cup.\n");
                    }
                } break;
                default:
                    ok = false;
            }
        }
    }
}
