package LAB7.ex1;

public class MaximumNbException extends Exception{
    int nb;
    public MaximumNbException(int nb,String msg){
        super(msg);
        this.nb = nb;
    }
    int getNb(){ return nb-1;}
}
