package LAB7.ex3;
import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int choice = -1;
        boolean ok = true;


        while(ok){
            System.out.println("For encrypting PRESS 1");
            System.out.println("For decrypting PRESS 2");
            System.out.println("For EXIT PRESS 0");

            choice = scanner.nextInt();
            switch(choice){
                case 1:
                    System.out.println("Encrypting! Type the name of the file!");
                    String file = scanner.next();
                    encrypt(file);break;

                case 2:
                    System.out.println("Encrypting! Type the name of the file!");
                    String file1 = scanner.next();
                    decrypt(file1);break;

                default : ok = false; break;
            }
        }



    }

    public static void encrypt(String file) throws IOException{
        BufferedReader br;
        BufferedWriter bw;
        try {

            br = new BufferedReader(new FileReader("src/LAB7/ex3/" + file + ".txt"));
            bw = new BufferedWriter(new FileWriter("src/LAB7/ex3/" + file + ".enc" ));


        } catch (IOException ex) {
            System.out.println("File has not been found.");
            return;
        }
        String S;
        while((S = br.readLine())!=null){
            for(int i = 0 ; i<S.length(); i++){
                char ch = (char) (S.charAt(i) +1);
                bw.write(ch);
            }

        }
        bw.close();

    }

    public static void decrypt(String file) throws IOException{
        BufferedReader br;
        BufferedWriter bw;
        try {

            br = new BufferedReader(new FileReader("src/LAB7/ex3/" + file + ".enc" ));
            bw = new BufferedWriter(new FileWriter("src/LAB7/ex3/" + file + ".dec" ));


        } catch (IOException ex) {
            System.out.println("File has not been found.");
            return;
        }
        String S;
        while((S = br.readLine())!=null){
            for(int i = 0 ; i<S.length(); i++){
                char ch = (char) (S.charAt(i)-1);
                bw.write(ch);
            }

        }
        bw.close();

    }


}